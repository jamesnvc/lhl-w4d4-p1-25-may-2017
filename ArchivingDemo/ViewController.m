//
//  ViewController.m
//  ArchivingDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "User.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UIDatePicker *birthdatePicker;
@property (strong,nonatomic) User* currentUser;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self loadCurrentUser]) {
        [self displayUserGreeting];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveInfo:(UIButton*)sender {
    self.currentUser = [[User alloc] initWithName:self.nameField.text
                                         birthday:_birthdatePicker.date];
    [self saveCurrentUser];
    [self displayUserGreeting];
    sender.hidden = YES;
    [self.nameField resignFirstResponder];
}

- (void)displayUserGreeting
{
    self.nameField.hidden = YES;
    self.birthdatePicker.hidden = YES;

    UILabel *greetingLabel = [[UILabel alloc] init];
    greetingLabel.translatesAutoresizingMaskIntoConstraints = NO;
    // should be using NSDateFormatter to show birthdate if we were doing this for real
    greetingLabel.text = [NSString stringWithFormat:@"Hi %@; your birthday is %@", self.currentUser.name, self.currentUser.birthdate];
    [self.view addSubview:greetingLabel];
    [greetingLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [greetingLabel.centerYAnchor constraintEqualToAnchor:self.view.centerYAnchor].active = YES;
}

- (void)saveCurrentUser
{
    // this will invoke encodeWithCoder:
    NSData *currentUserData = [NSKeyedArchiver archivedDataWithRootObject:self.currentUser];
    [[NSUserDefaults standardUserDefaults]
     setObject:currentUserData forKey:@"currentUser"];
}

- (BOOL)loadCurrentUser
{
    NSData *saved = [[NSUserDefaults standardUserDefaults]
                     objectForKey:@"currentUser"];
    if (saved != nil) {
        // this will invoke initWithCoder:
        self.currentUser = [NSKeyedUnarchiver unarchiveObjectWithData:saved];
        return YES;
    }
    return NO;
}

@end
