//
//  User.h
//  ArchivingDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject <NSCoding>

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSDate *birthdate;

- (instancetype)initWithName:(NSString*)name birthday:(NSDate*)birthdate;

@end
