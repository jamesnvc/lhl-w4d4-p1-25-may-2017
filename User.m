//
//  User.m
//  ArchivingDemo
//
//  Created by James Cash on 25-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "User.h"

@implementation User

- (instancetype)initWithName:(NSString*)name birthday:(NSDate*)birthdate
{
    if ([super init]) {
        _name = name;
        _birthdate = birthdate;
    }
    return self;
}

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.birthdate forKey:@"birthdate"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ([super init]) {
        _name = [aDecoder decodeObjectForKey:@"name"];
        _birthdate = [aDecoder decodeObjectForKey:@"birthdate"];
    }
    return self;
}

@end
